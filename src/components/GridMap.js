import React, { Component } from "react";
import "./GridMap.css";

class FlexMap extends Component {
  state = {
    zoomLevel: 0,
    canvasRef: React.createRef(),
    canHeight: 0,
    canWidth: 0,
    image_extension: ".jpg",
    ctx: {}
  };
  componentDidMount() {
    const ctx =
      this.state.canvasRef.current &&
      this.state.canvasRef.current.getContext("2d");
    ctx.imageSmoothingEnabled = true;

    this.setState({ ctx }, () => {
      this.processImages();
    });
    window.addEventListener("keypress", e => {
      if (e.key === "+") {
        this.zoomInHandler();
      }
    });
    window.addEventListener("keypress", e => {
      if (e.key === "-") {
        this.zoomOutHandler();
      }
    });
  }

  getTileCoordinates() {
    const dimension = Math.pow(2, this.state.zoomLevel);
    const imageCoordinates = [];
    for (let xCoord = 0; xCoord < dimension; xCoord++) {
      for (let yCoord = 0; yCoord < dimension; yCoord++) {
        const tilePath =
          "tiles/" +
          this.state.zoomLevel +
          "/" +
          xCoord +
          "/" +
          yCoord +
          this.state.image_extension;
        const tile = new Image();
        tile.src = tilePath;
        tile.alt = tilePath;
        const coordinate = { x: xCoord, y: yCoord, tile: tile };
        imageCoordinates.push(coordinate);
      }
    }

    return imageCoordinates;
  }
  drawImages = (tiles, coordinates) => {
    const dimension = Math.pow(2, this.state.zoomLevel);
    // var dpr = window.devicePixelRatio;
    for (let i = 0; i < tiles.length; i++) {
      const xLoc = coordinates[i].x * tiles[i].naturalWidth;
      const yLoc = coordinates[i].y * tiles[i].naturalHeight;
      this.setState(
        state => {
          return {
            canHeight: dimension * tiles[i].naturalHeight,
            canWidth: dimension * tiles[i].naturalWidth
          };
        },
        () => {
          const ctx =
            this.state.canvasRef.current &&
            this.state.canvasRef.current.getContext("2d");
          ctx.imageSmoothingEnabled = true;
          ctx.drawImage(tiles[i], xLoc, yLoc);
          ctx.canvas.style.transition = "width 2s ease 1s,height 2s ease 1s";
          ctx.canvas.style.width = this.state.canWidth;
          ctx.canvas.style.height = this.state.canHeight;
        }
      );
    }
  };

  processImages = async () => {
    const coordinates = this.getTileCoordinates();
    const imgOnloadPromises = this.getAllOnLoadImgPromises(coordinates);
    const resolvedOnloadImgPromises = await Promise.all(imgOnloadPromises);
    this.drawImages(resolvedOnloadImgPromises, coordinates);
  };

  promiseImageLoad = tile => {
    return new Promise((fulfill, reject) => {
      tile.onload = () => {
        fulfill(tile);
      };
    });
  };
  getAllOnLoadImgPromises = coordinates => {
    const x = [];
    coordinates.forEach(coordinate => {
      x.push(this.promiseImageLoad(coordinate.tile));
    });
    return x;
  };

  zoomInHandler = async e => {
    if (this.state.zoomLevel < 3) {
      this.setState(
        state => {
          return { zoomLevel: state.zoomLevel + 1 };
        },
        async () => {
          this.processImages();
        }
      );
    }
  };
  zoomOutHandler = async () => {
    if (this.state.zoomLevel > 0) {
      this.setState(
        state => {
          return { zoomLevel: state.zoomLevel - 1 };
        },
        async () => {
          this.processImages();
        }
      );
    }
  };

  animate = () => {};
  render() {
    return (
      <React.Fragment>
        <div>
          <button className="ZoomInBtn" onClick={this.zoomInHandler}>
            + ZoomIn
          </button>
          <button className="ZoomOutBtn" onClick={this.zoomOutHandler}>
            - ZoomOut
          </button>
          <p>{`Zoom level: ${this.state.zoomLevel}`}</p>
        </div>
        <br />
        <div className="Container_0">
          <div className="CanDiv">
            <canvas
              onChange={console.log("changed")}
              className="Zoom"
              ref={this.state.canvasRef}
              width={this.state.canWidth}
              height={this.state.canHeight}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default FlexMap;
