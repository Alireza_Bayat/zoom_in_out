import React, { Component } from "react";
import "./App.css";
import GridMap from "./components/GridMap";

class App extends Component {
  render() {
    return (
      <div className="App">
        <GridMap />
      </div>
    );
  }
}

export default App;
